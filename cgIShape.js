//
// fill in code that creates the triangles for a cube with dimensions 1x1x1
// on each side (and the origin in the center of the cube). with an equal
// number of subdivisions along each cube face as given by the parameter
//subdivisions
//


function makeCube (subdivisions) {
    let a=0;
    let b=0;
    let x=0.5;
    let y=0.5;
    let z=0.5;
    while(a<subdivisions){
        for(b=0;b<subdivisions;b++){
            x0=b*(1/subdivisions)-x;
            y0=y-a*(1/subdivisions);
            z0=a*(1/subdivisions)-z;
            x1=(b+1)*(1/subdivisions)-x;
            y1=y-a*(1/subdivisions);
            z1=a*(1/subdivisions)-z;
            x2=b*(1/subdivisions)-x;
            y2=y-(a+1)*(1/subdivisions);
            z2=(a+1)*(1/subdivisions)-z;
            x3=(b+1)*(1/subdivisions)-x;
            y3=y-(a+1)*(1/subdivisions);
            z3=(a+1)*(1/subdivisions)-z;
            makeTriangle(x0,y0,-z,x1,y1,-z,x2,y2,-z);
            makeTriangle(x1,y1,-z,x3,y3,-z,x2,y2,-z);
            makeTriangle(x0,y0,z,x1,y1,z,x2,y2,z);
            makeTriangle(x1,y1,z,x3,y3,z,x2,y2,z);
            makeTriangle(x0,y,z0,x1,y,z1,x2,y,z2);
            makeTriangle(x1,y,z1,x3,y,z3,x2,y,z2);
            makeTriangle(x0,-y,z0,x1,-y,z1,x2,-y,z2);
            makeTriangle(x1,-y,z1,x3,-y,z3,x2,-y,z2);
            makeTriangle(-x,y0,z0,-x,y1,z1,-x,y2,z2);
            makeTriangle(-x,y1,z1,-x,y3,z3,-x,y2,z2);
            makeTriangle(x,y0,z0,x,y1,z1,x,y2,z2);
            makeTriangle(x,y1,z1,x,y3,z3,x,y2,z2);
        }
        a++;
    }
}

function makeTriangle(x0,y0,z0,x1,y1,z1,x2,y2,z2){
    addTriangle(x0,y0,z0,x1,y1,z1,x2,y2,z2);
    addTriangle(x0,y0,z0,x2,y2,z2,x1,y1,z1);
}




//
// fill in code that creates the triangles for a cylinder with diameter 1
// and height of 1 (centered at the origin) with the number of subdivisions
// around the base and top of the cylinder (given by radialdivision) and
// the number of subdivisions along the surface of the cylinder given by
//heightdivision.
//



//
// fill in code that creates the triangles for a cone with diameter 1
// and height of 1 (centered at the origin) with the number of
// subdivisions around the base of the cone (given by radialdivision)
// and the number of subdivisions along the surface of the cone
//given by heightdivision.
//
function makeCone (radialdivision,heightdivision){
    let a=0;
    let b=0;
    let y=0.5;
    let x=0;
    let z=0;
    while(a<heightdivision){
        for(b=0;b<radialdivision;b++){
            x0=Math.cos(2*Math.PI*b/radialdivision);
            z0=Math.sin(2*Math.PI*b/radialdivision);
            y0=y-a*(1/heightdivision);
            x1=Math.cos(2*Math.PI*(b+1)/radialdivision);
            z1=Math.sin(2*Math.PI*(b+1)/radialdivision);
            y1=y-a*(1/heightdivision);
            x2=Math.cos(2*Math.PI*b/radialdivision);
            z2=Math.sin(2*Math.PI*b/radialdivision);
            y2=y-(a+1)*(1/heightdivision);
            x3=Math.cos(2*Math.PI*(b+1)/radialdivision);
            z3=Math.sin(2*Math.PI*(b+1)/radialdivision);
            y3=y-(a+1)*(1/heightdivision);
            makeTriangle(x0,y0,z0,x1,y1,z1,x2,y2,z2);
            makeTriangle(x1,y1,z1,x3,y3,z3,x2,y2,z2);
        }
        a++;
    }
}
    
//
// fill in code that creates the triangles for a sphere with diameter 1
// (centered at the origin) with number of slides (longitude) given by
// slices and the number of stacks (lattitude) given by stacks.
// For this function, you will implement the tessellation method based
// on spherical coordinates as described in the video (as opposed to the
//recursive subdivision method).
//
function makeSphere (slices, stacks) {
    // fill in your code here.
}


////////////////////////////////////////////////////////////////////
//
//  Do not edit below this line
//
///////////////////////////////////////////////////////////////////

function radians(degrees)
{
  var pi = Math.PI;
  return degrees * (pi/180);
}

function addTriangle (x0,y0,z0,x1,y1,z1,x2,y2,z2) {

    
    var nverts = points.length / 4;
    
    // push first vertex
    points.push(x0);  bary.push (1.0);
    points.push(y0);  bary.push (0.0);
    points.push(z0);  bary.push (0.0);
    points.push(1.0);
    indices.push(nverts);
    nverts++;
    
    // push second vertex
    points.push(x1); bary.push (0.0);
    points.push(y1); bary.push (1.0);
    points.push(z1); bary.push (0.0);
    points.push(1.0);
    indices.push(nverts);
    nverts++
    
    // push third vertex
    points.push(x2); bary.push (0.0);
    points.push(y2); bary.push (0.0);
    points.push(z2); bary.push (1.0);
    points.push(1.0);
    indices.push(nverts);
    nverts++;
}

